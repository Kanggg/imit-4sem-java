package ru.omsu.imit.kang.pakech4;

public interface Executable {
    void execute();
}
