package ru.omsu.imit.kang.pakech5.predicates.lambdas;

import ru.omsu.imit.kang.pakech5.models.Human;

@FunctionalInterface
public interface AgeGrowPredicate {
    Human addYear(Human h);
}
