package ru.omsu.imit.kang.pakech5.predicates.lambdas;

import ru.omsu.imit.kang.pakech5.models.Human;

@FunctionalInterface
public interface SurnamePredicate<T extends Human> {
    Boolean hasSame(T p1, T p2);
}
