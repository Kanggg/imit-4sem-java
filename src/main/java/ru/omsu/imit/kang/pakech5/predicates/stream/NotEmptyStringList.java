package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;

@FunctionalInterface
public interface NotEmptyStringList {
    List<String> get(List<String> list);
}
