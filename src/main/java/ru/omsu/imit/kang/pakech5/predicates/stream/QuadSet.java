package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;

@FunctionalInterface
public interface QuadSet {
    List<Integer> get(Integer[] list);
}
