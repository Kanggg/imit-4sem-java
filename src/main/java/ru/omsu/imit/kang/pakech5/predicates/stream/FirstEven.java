package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;

@FunctionalInterface
public interface FirstEven {
    Integer get(List<Integer> list);
}
