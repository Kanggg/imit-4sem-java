package ru.omsu.imit.kang.pakech5.models;

public enum Gender {
    MALE,
    FEMALE
}
