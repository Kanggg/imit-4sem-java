package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;

public interface LastElements {
    List<Object> get(List<Object> list);
}
