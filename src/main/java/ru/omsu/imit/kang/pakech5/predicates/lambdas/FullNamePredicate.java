package ru.omsu.imit.kang.pakech5.predicates.lambdas;

import ru.omsu.imit.kang.pakech5.models.Human;

public interface FullNamePredicate<T extends Human> {
    String get(T person);
}
