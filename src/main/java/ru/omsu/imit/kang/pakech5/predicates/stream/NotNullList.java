package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;

@FunctionalInterface
public interface NotNullList {
    List<Object> get(List<Object> list);
}
