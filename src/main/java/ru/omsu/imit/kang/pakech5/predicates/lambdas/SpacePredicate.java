package ru.omsu.imit.kang.pakech5.predicates.lambdas;

@FunctionalInterface
public interface SpacePredicate {
    Boolean hasSpacing(String s);
}
