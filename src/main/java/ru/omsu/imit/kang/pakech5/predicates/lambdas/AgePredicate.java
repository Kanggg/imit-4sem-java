package ru.omsu.imit.kang.pakech5.predicates.lambdas;


import ru.omsu.imit.kang.pakech5.models.Human;

@FunctionalInterface
public interface AgePredicate<T extends Human> {
    Integer get(T person);
}
