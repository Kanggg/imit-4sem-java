package ru.omsu.imit.kang.pakech5.predicates.stream;


import ru.omsu.imit.kang.pakech5.models.Human;
import java.util.Collection;

public interface GenderAgeSorted {
    Collection<Human> get(Collection<Human> collection);
}
