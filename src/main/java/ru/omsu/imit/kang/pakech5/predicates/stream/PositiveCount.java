package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.Set;

@FunctionalInterface
public interface PositiveCount {
    long get(Set<Integer> list);
}
