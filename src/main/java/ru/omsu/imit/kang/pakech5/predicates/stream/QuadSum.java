package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.Set;

@FunctionalInterface
public interface QuadSum {
    Integer get(Set<Integer> set);
}
