package ru.omsu.imit.kang.pakech5.predicates.stream;

import java.util.List;
import java.util.Set;

@FunctionalInterface
public interface SetToSortedList {
    List<String> get(Set<String> set);
}
