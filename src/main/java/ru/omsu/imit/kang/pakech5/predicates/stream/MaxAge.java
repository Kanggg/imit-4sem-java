package ru.omsu.imit.kang.pakech5.predicates.stream;

import ru.omsu.imit.kang.pakech5.models.Human;
import java.util.Collection;

@FunctionalInterface
public interface MaxAge {
    Integer get(Collection<Human> collection);
}
