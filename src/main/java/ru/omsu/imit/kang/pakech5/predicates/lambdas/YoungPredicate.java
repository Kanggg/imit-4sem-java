package ru.omsu.imit.kang.pakech5.predicates.lambdas;

import ru.omsu.imit.kang.pakech5.models.Human;

@FunctionalInterface
public interface YoungPredicate {
    Boolean isAllYounger(Human h1, Human h2, Human h3, int maxAge);
}
