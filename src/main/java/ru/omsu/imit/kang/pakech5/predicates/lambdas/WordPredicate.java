package ru.omsu.imit.kang.pakech5.predicates.lambdas;

@FunctionalInterface
public interface WordPredicate {
    Integer getWordCount(String s);
}
