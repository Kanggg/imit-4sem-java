package ru.omsu.imit.kang.pakech1;

import java.io.*;
import java.util.Scanner;

public class DemoMatrix {
    private static final String MAT1_PATH = "/matrices/matrix.txt";
    private static final String MAT2_PATH = "/matrices/invertibleMatrix.txt";

    private static Reader getResourceFileReader(final String fileName) {
        InputStream stream = DemoMatrix.class.getResourceAsStream(fileName);
        return stream == null ? null : new InputStreamReader(stream);
    }

    public static void printMatr(final IMatrix mat) {
        final int size = mat.getLen();
        System.out.println(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.format("%10.4f ", mat.getVal(i, j));
            }
            System.out.println();
        }
        System.out.flush();
    }

    public static IMatrix readMatr(final Reader inp) {
        Scanner sc = new Scanner(inp);
        final int size = sc.nextInt();
        IMatrix mat = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                mat.setVal(i, j, sc.nextDouble());
            }
        }
        return mat;
    }

    public static double getSum(IMatrix matr) {
        double sum = 0.0;
        for (int i = 0; i < matr.getLen(); i++) {
            for (int j = 0; j < matr.getLen(); j++) {
                sum += matr.getVal(i, j);
            }
        }
        return sum;
    }

    private static void interactiveDemo(IMatrix mat) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter row and col (<= " + mat.getLen() +"): ");
        int i = sc.nextInt(), j = sc.nextInt();
        System.out.print("Enter double value: ");
        double val = sc.nextDouble();

        System.out.println("\nOld elem = " + mat.getVal(i, j));
        mat.setVal(i, j, val);
        System.out.println("New elem = " + mat.getVal(i, j));
    }

    public static void main(String[] args) {
        IMatrix mat1;
        InvertableMatrix mat2;

        // read matrices from resources
        try {
            mat1 = readMatr(getResourceFileReader(MAT1_PATH));
            mat2 = new InvertableMatrix(readMatr(getResourceFileReader(MAT2_PATH)));
        } catch (IllegalArgumentException e) {
            System.err.println("Wrong argument: " + e.getMessage());
            return;
        }

        // print matrices
        System.out.println("mat1:");
        printMatr(mat1);
        System.out.println("det = " + mat1.getDet());

        System.out.println("\nmat2:");
        printMatr(mat2);
        System.out.println("det = " + mat2.getDet());

        // execute methods
        try {
            // sumAllElements()
            double sum = getSum(mat1);
            System.out.println("\nSum of all mat1 elements: " + sum);

            // .invert()
            IInvertableMatrix iMat2 = mat2.getInvMatr();
            System.out.println("\nInverted mat2:");
            printMatr(iMat2);
            System.out.println("det = " + iMat2.getDet());

            System.out.println("\nModify mat1:");
            interactiveDemo(mat1);
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Out of bounds: " + e.getMessage());
        }

        // serialize and deserialize `mat1`
        try {
            File tempFile = File.createTempFile("matrix", ".bin");

            ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(tempFile));
            objOut.writeObject(mat1);
            objOut.close();

            ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(tempFile));
            IMatrix mat1d = (Matrix)objIn.readObject();
            objIn.close();

            System.out.println("\nDeserialized mat1: (" + tempFile.toString() + ")");
            printMatr(mat1d);
            System.out.println("(mat1 == mat1d) is " + (mat1.equals(mat1d)));
        } catch (IOException e) {
            System.err.println("I/O error while matrix serialization or deserialization: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println("Cant find deserialized class: " + e.getMessage());
        }
    }

}
