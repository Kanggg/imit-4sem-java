package ru.omsu.imit.kang.pakech1;

public class InvertableMatrix extends Matrix implements IInvertableMatrix {


    public InvertableMatrix(int N) {
        this.N = N;
        arr = new double[N*N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == j)
                    setVal(i, j, 1.0);
                else
                    setVal(i, j, 0.0);
            }
        }
    }

    public InvertableMatrix(IMatrix matr) {
        N = matr.getLen();
        arr = new double[N*N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                arr[i * N + j] = matr.getVal(i, j);
            }
        }
    }

    public IInvertableMatrix getInvMatr() {
        IInvertableMatrix m = new InvertableMatrix(advTranspose());
        if (getDet() != 0) {
            getDet();
            System.out.println("det = " + lastDet);

            for (int i = 0; i < getLen(); i++) {
                for (int j = 0; j < getLen(); j++) {
                    m.setVal(i, j, m.getVal(i, j) / Math.abs(lastDet));
                }
            }
        } else {
            // обратной матрицы небудет
        }

        return m;
    }

    public Matrix advTranspose() {
        Matrix res = new Matrix(getLen());
        double temp;
        for (int i = 0; i < getLen(); i++) {
            for (int j = i; j < getLen(); j++) {
                temp = getVal(i, j);
                setVal(i, j, getVal(j, i));
                setVal(j, i, temp);
            }
        }

        for (int i = 0; i < getLen(); i++) {
            for (int j = 0; j < getLen(); j++) {
                res.setVal(i, j, Math.pow(-1,(i+j+1%2))* minor(i, j).getDet());
            }
        }
        return res;
    }
}
