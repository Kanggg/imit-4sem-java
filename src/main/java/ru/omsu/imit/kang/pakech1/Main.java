package ru.omsu.imit.kang.pakech1;

public class Main {
    //UNDONE 5.2

    public static void main(String[] args) {
        Matrix matr = new Matrix(4);
        matr.setVal(0,0, 7.0);
        matr.setVal(0,1, 1.0);
        matr.setVal(0,2, 3.0);
        matr.setVal(0,3, 8.0);
        matr.setVal(1,0, -5.0);
        matr.setVal(1,1, 4.0);
        matr.setVal(1,2, 2.0);
        matr.setVal(1,3, 9.0);
        matr.setVal(2,0, -7.0);
        matr.setVal(2,1, 2.0);
        matr.setVal(2,2, 8.0);
        matr.setVal(2,3, 3.0);
        matr.setVal(3,0, 6.0);
        matr.setVal(3,1, 9.0);
        matr.setVal(3,2, 4.0);
        matr.setVal(3,3, 5.0);
        DemoMatrix.printMatr(matr);
        InvertableMatrix im = new InvertableMatrix(matr);
        DemoMatrix.printMatr(im.getInvMatr());
    }
}

