package ru.omsu.imit.kang.pakech1;

public class MatrixException extends Exception {  //5
    public MatrixException(String message) {
        super(message);
    }
}
