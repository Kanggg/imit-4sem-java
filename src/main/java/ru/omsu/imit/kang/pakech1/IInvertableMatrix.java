package ru.omsu.imit.kang.pakech1;

public interface IInvertableMatrix extends IMatrix { // 2
    IInvertableMatrix getInvMatr();
    double getDet();

}
