package ru.omsu.imit.kang.pakech1;

public interface IMatrix {
    double getVal(int i, int j);
    void setVal(int i, int j, double val);
    double getDet();
    int getLen();
}
