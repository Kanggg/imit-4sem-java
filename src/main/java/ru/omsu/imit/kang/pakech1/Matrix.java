package ru.omsu.imit.kang.pakech1;

import java.io.Serializable;
import java.util.Arrays;

public class Matrix implements IMatrix, Serializable { //3
    protected int N;
    protected double[] arr;
    protected boolean checkDet;
    protected double lastDet;

    public Matrix(){
        this(1);
    }

    public Matrix(int N) {
        this.N = N;
        checkDet = false;
        arr = new double[N*N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                arr[i * N + j] = 0.0;
            }
        }
    }

    public Matrix(Matrix matr) {
        N = matr.N;
        checkDet = false;
        arr = new double[N*N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                arr[i * N + j] = matr.getVal(i, j);
            }
        }
    }

    public double getDet() {
        if (checkDet) {
            return lastDet;
        }
        double res = 0.0;
        double temp = 1.0; // -1
        if (N == 1) {
            return getVal(0, 0);
        } else if (getLen() == 2) {
            return getVal(0, 0) * getVal(1, 1) - getVal(0, 1) * getVal(1, 0);
        } else {
            for (int i = 0; i < N; i++) {
                res += getVal(0, i) * minor(0, i).getDet() * Math.pow(-1.0,i);
            }
        }
        lastDet = res;
        checkDet = true;
        return res;
    }

    public Matrix minor(int a, int b) {
        Matrix n = new Matrix(N - 1);
        int leftCheck = 0;
        int upCheck = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == a || j == b) {
                    if(i == a){
                        upCheck = 1;
                    } else { leftCheck = 1; }
                } else { n.setVal(i - upCheck, j - leftCheck , getVal(i, j)); }
            }
            leftCheck = 0;
        }
        return n;
    }

    //////////////////////////////////////Getters setters and hashcode

    public double getVal(int i, int j){
        return arr[i * N + j];
    }

    public void setVal(int i, int j, double val){
        arr[i * N + j] = val;
        checkDet = false;
    }

    public int getLen(){
        return N;
    }

    public double getLastDet(){
        return lastDet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return N == matrix.N &&
                Arrays.equals(arr, matrix.arr);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }
}
