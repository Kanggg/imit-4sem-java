package ru.omsu.imit.kang.pakech3;

import au.com.bytecode.opencsv.CSVWriter;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StreamIO {
    // UNDONE все допы, кроме 7


    public static void  writeIntArrayToBinaryFile(File file, int[] array) {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            for (int elem: array) {
                dos.writeInt(elem);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static int[] readIntArrayFromBinaryFile(File file, int n) {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            if (n > dis.available()) {
                throw new IOException("Index out of bound");
            }
            int[] res = new int[n];

            for (int i = 0; i < n; i++) {
                res[i] = dis.readInt();
            }
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void  writeCharArrayToBinaryFile(File file, int[] array) {
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            for (int elem: array) {
                raf.writeUTF(Integer.toString(elem));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] readCharArrayFromBinaryFile(File file, int n) {
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            int[] res = new int[n];
            for (int i = 0; i < n; i++) {
                res[i] = Integer.valueOf(raf.readUTF());
                //raf.readUTF();
            }
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int[]  readIntArrayFromPositionFromBinaryFile(File file, int position, int n) {
        int[] res = null;
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            if (position > file.length()/4) {
                return null;
            }
            res = new int[(int)file.length()/4 - position];
            raf.seek(position * 4);
            for (int i = 0; i < n; i++) {
                res[i] = raf.readInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }


    // Ignored
    public static List<String> getFilesWithCatalogues(String expancion, String catalogue) { // TODO закончить 5
        List<String> list = new ArrayList<>();
        File dir = new File(catalogue);
        String[] filenames = dir.list();
        //System.out.println(filenames);
        assert filenames != null;
        if (filenames == null) {
            return null;
        }
        for (String filename : filenames) {
            if (filename.toLowerCase().endsWith(expancion)) {
                list.add(filename);
                System.out.println(filename);
            } else{
                File tempfile = new File(filename);
                //System.out.println(tempfile);
                if(tempfile.isDirectory()){
                    catalogue += "\\"  + filename;
                    List<String> tempList = getFilesWithCatalogues(expancion, catalogue);
                    for (String string: tempList) { //то рекурсивный вызов этой функции
                        list.add(string);
                    }
                }
            }
        }
        return list;
    }

    public static List<String> getListWithFileExpancion(String expancion, String catalogue) {
        List<String> list = new ArrayList<>();
        File dir = new File(catalogue);
        String[] filenames = dir.list();
        assert filenames != null;
        for (String filename : filenames) {
            if (filename.toLowerCase().endsWith(expancion))
                list.add(filename);
        }
        return list;
    }

    public static void saveHouseToCSV(House house) {
        try(CSVWriter writer = new CSVWriter(new FileWriter("House_" + house.getNumber() + ".csv"),';')) {
            writer.writeNext(new String[]{"Данные о доме"});
            writer.writeNext(new String[]{"Кадастровый номер:", house.getNumber()});
            writer.writeNext(new String[]{"Адрес:", house.getAdress()});
            writer.writeNext(new String[]{"Старший по дому:", house.getSenior().getFirstName(), house.getSenior().getLastName(), house.getSenior().getMiddleName()});
            writer.writeNext(new String[]{"Данные о квартирах"});
            writer.writeNext(new String[]{"№", "Площадь, кв.м", "Владельцы"});
            for (Flat flat: house.getList()) {
                writer.writeNext(new String[]{Integer.toString(flat.getNumber()), Integer.toString(flat.getSquare()), flat.getPersonsFIO()});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void serializeHouseToBinaryFile(File file, House house) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House deserializeHouseFromBinaryFile(File file) {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (House)ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeHouse(File file, House house) {
        try(FileOutputStream fos = new FileOutputStream(file)) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(fos, house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House readHouse(File file) {
        try(FileInputStream fis = new FileInputStream(file)) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(fis, House.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
