package ru.omsu.imit.kang.pakech3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String number;
    private String adress;
    private Person senior;
    private List<Flat> list;

    public House() {}

    public House(String number, String adress, Person senior) {
        this.number = number;
        this.adress = adress;
        this.senior = senior;
        list = new ArrayList<>();
    }

    public House(String number, String adress, Person senior, List<Flat> list) {
        this.number = number;
        this.adress = adress;
        this.senior = senior;
        this.list = list;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Person getSenior() {
        return senior;
    }

    public void setSenior(Person senior) {
        this.senior = senior;
    }

    public List<Flat> getList() {
        return list;
    }

    public void setList(List<Flat> list) {
        this.list = list;
    }

//    public String getPersonsFIO() {
//        StringBuilder sb = new StringBuilder();
//        for (Flat flat: list) {
//            sb.append(flat.getPersonsFIO());
//        }
//        return sb.toString();
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(number, house.number) &&
                Objects.equals(adress, house.adress) &&
                Objects.equals(senior, house.senior) &&
                Objects.equals(list, house.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, adress, senior, list);
    }
}
