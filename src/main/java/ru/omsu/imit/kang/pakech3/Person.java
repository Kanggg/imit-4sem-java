package ru.omsu.imit.kang.pakech3;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.omsu.imit.kang.pakech4.Executable;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Executable {
    private String firstName;
    private String lastName;
    private String middleName;
    private int day;
    private int month;
    private int year;

    public void execute() {
        System.out.println("Executed from " + this.getClass().getName());
    }

    public Person() {}

    public Person(String firstName, String lastName, String middleName, int day, int month, int year) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Person(Person p) {
        this(p.getFirstName(), p.getLastName(), p.getMiddleName(), p.getDay(), p.getMonth(), p.getYear());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @JsonIgnore
    public String getFIO() {
        return middleName + " " + firstName.charAt(0) + ". " + lastName.charAt(0) + ".";
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return day == person.day &&
                month == person.month &&
                year == person.year &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(middleName, person.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middleName, day, month, year);
    }
}
