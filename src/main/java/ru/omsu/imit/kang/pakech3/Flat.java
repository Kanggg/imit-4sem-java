package ru.omsu.imit.kang.pakech3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {
    private int number;
    private int square;
    private List<Person> list;

    public Flat(int number, int square) {
        this.number = number;
        this.square = square;
        list = new ArrayList<>();
    }

    public Flat(int number, int square, List<Person> list) {
        this.number = number;
        this.square = square;
        this.list = list;
    }

    public Flat(Flat flat) {
        this.number = flat.getNumber();
        this.square = flat.getSquare();
        this.list = flat.getList();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public List<Person> getList() {
        return list;
    }

    public void setList(List<Person> list) {
        this.list = list;
    }

    public String getPersonsFIO() {
        StringBuilder sb = new StringBuilder();
        if (list.size() > 1) {
            for (int i = 0; i < list.size()-1; i++) {
                sb.append(list.get(i).getFIO()+ ", ");
            }
            sb.append(list.get(list.size()-1).getFIO());
            return sb.toString();
        }
        sb.append(list.get(0).getFIO());
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                square == flat.square &&
                Objects.equals(list, flat.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, square, list);
    }
}
