package ru.omsu.imit.kang.pakech2;

import java.util.*;

public class CollectionsDemo {

    public static int getSumStringsFirstCharInList
                (List<String> set, char sign) { // 1
        int sum = 0;
        for (String string: set) {
            if (string.charAt(0) == sign) {
                sum++;
            }
        }
        return sum;
    }

    public static List<Human> getListEqualLastName
                    (List<Human> peoples, Human human) { // 2
        List<Human> res = new ArrayList<Human>();
        for (Human people: peoples) {
            if (people.getLastName().equals(human.getLastName())) {
                res.add(people);
            }
        }
        return res;
    }

    public static List<Human> getListWithoutHuman
                    (List<Human> peoples, Human human) { // 3
        List<Human> res = new ArrayList<Human>();
        for (Human arr: peoples) {
            if (!arr.equals(human)) {
                res.add(new Human(arr));
            }
        }
        return res;
    }

    public static List<Set<Integer>> getSetIntegerWithoutIntersection
                    (List<Set<Integer>> list, Set<Integer> set) { // 4
        List<Set<Integer>> res = new ArrayList<Set<Integer>>();
        boolean checkCont = false;
        for (Set<Integer> i: list) {

            for (Integer j: set) {
                if (i.contains(j)) {
                    checkCont = true;
                }
            }

            if (checkCont) {
                //DO NOTHING
            } else {
                res.add(i);
            }

            checkCont = false;
        }
        return res;
    }

    public static <T extends Human> Set<T> getSetMaxAge
                        (List<T> humans) { // 5
        int maxAge = 0;

        for(T human: humans) {
            maxAge = Math.max(maxAge, human.getAge());
        }
        Set<T> res = new HashSet<T>();
        for(T human: humans) {
            if (human.getAge() == maxAge) {
                res.add(human);
            }
        }
        return res;
    }

    public static Set<Human> getSetOnIndex
                (Map<Integer, Human> humans, Set<Integer> set) { // 7
        Set<Human> res = new HashSet<Human>();
        for(Integer key: set) {
            if (humans.containsKey(key)) {
                res.add(humans.get(key));
            }
        }
        return res;
    }

    public static Set<Integer> getSetHumansUpper18
                        (Map<Integer, Human> humans) {
        Set<Integer> set = new HashSet<Integer>();
        for (Map.Entry<Integer, Human> pair: humans.entrySet()) {
            if (pair.getValue().getAge() > 17) {
                set.add(pair.getKey());
            }
        }
        return set;
    }

    public static Map<Integer, Integer> getMapKeyIsAge
                            (Map<Integer, Human> humans) {
        Map<Integer, Integer> res = new HashMap<Integer, Integer>();
        for (Map.Entry<Integer, Human> pair: humans.entrySet()) {
            res.put(pair.getKey(), pair.getValue().getAge());
        }
        return res;
    }

    public static Map<Integer, List<Human>> getMapAgeList
                                        (Set<Human> humans) {
        Map<Integer, List<Human>> res = new HashMap<Integer, List<Human>>();
        int cnt = 0;
        List<Human> list = new ArrayList<Human>();
        Set<Integer> used = new HashSet<Integer>();
        while (cnt < humans.size()) {
            for (Human human: humans) {
                if (!used.contains(human.getAge())) {
                    if (list.isEmpty()) {
                        list.add(human);
                        cnt++;
                    } else {
                        if (human.getAge() == list.get(0).getAge()) {
                            list.add(human);
                            cnt++;
                        }
                    }
                }
            }
            res.put(list.get(0).getAge(), new ArrayList<Human>(list));
            used.add(list.get(0).getAge());
            list.clear();
        }
        return res;
    }
}
