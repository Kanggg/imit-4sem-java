package ru.omsu.imit.kang.pakech2;

public class Student extends Human { // 5
    private String faculty;

    public Student(String firstName, String lastName,
                   String middleName, int age, String faculty) {
        super(firstName, lastName, middleName, age);
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
}
