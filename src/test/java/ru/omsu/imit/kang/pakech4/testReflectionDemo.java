package ru.omsu.imit.kang.pakech4;

import org.junit.Test;
import ru.omsu.imit.kang.pakech2.Human;
import ru.omsu.imit.kang.pakech2.Student;
import ru.omsu.imit.kang.pakech3.Person;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class testReflectionDemo {
    @Test
    public void testCountHuman() {
        List<Object> objectList = new ArrayList<>();
        objectList.add(new Human("AAA", "AAA", "AAA", 1998));
        objectList.add(new Human("BBB", "BBB", "BBB", 1997));
        objectList.add(new Human("CCC", "CCC", "CCC", 1995));
        objectList.add(new Student("CCC", "CCC", "CCC", 1995,"facul"));
        objectList.add(new Object());
        objectList.add(new Object());
        objectList.add("someString");
        assertEquals(ReflectionDemo.countHuman(objectList), 4);
    }
    @Test
    public void testListName(){
        List<String> result = new ArrayList<>();
        Person person = new Person("abc", "aa", "avd", 21, 11, 1594);
        result.add("getFirstName");
        result.add("setFirstName");
        result.add("getLastName");
        result.add("getFIO");
        result.add("setLastName");

        result.add("getMiddleName");
        result.add("setMiddleName");
        result.add("getDay");
        result.add("setDay");
        result.add("getMonth");
        result.add("setMonth");
        result.add("getYear");
        result.add("setYear");
        result.add("equals");
        result.add("hashCode");

        assertTrue(ReflectionDemo.listName(person).containsAll(result));
    }

    @Test
    public void testSuperClassName(){
        List<String> result = new ArrayList<>();
        Student student = new Student("AAA","BBB","CCC",90,"IMIT");
        result.add("Human");
        result.add("Object");
        assertTrue(result.containsAll(ReflectionDemo.superClassName(student)));
    }
    @Test
    public void testExecutable(){
        List<Object> list = new ArrayList<>();
        list.add(new ReflectionDemo());
        list.add(new Object());
        list.add(new Person("AAA", "AAA", "AAA", 12, 34, 56));
        list.add(new Student("AAA","BBB","CCC",90,"IMIT"));
        list.add(new Object());
        list.add("someStr");
        assertEquals(ReflectionDemo.findExecutable(list), 2);
    }

    @Test
    public void testGetSetAndGet(){
        List<String> list = new ArrayList();
        list.add("public java.lang.String ru.omsu.imit.kang.pakech2.Human.getLastName()");
        list.add("public java.lang.String ru.omsu.imit.kang.pakech2.Human.getFirstName()");
        list.add("public void ru.omsu.imit.kang.pakech2.Human.setFirstName(java.lang.String)");
        list.add("public void ru.omsu.imit.kang.pakech2.Human.setLastName(java.lang.String)");
        list.add("public java.lang.String ru.omsu.imit.kang.pakech2.Human.getMiddleName()");
        list.add("public void ru.omsu.imit.kang.pakech2.Human.setMiddleName(java.lang.String)");
        list.add("public int ru.omsu.imit.kang.pakech2.Human.getAge()");
        list.add("public void ru.omsu.imit.kang.pakech2.Human.setAge(int)");
        list.add("public final native java.lang.Class java.lang.Object.getClass()");
        System.out.println(ReflectionDemo.getGetAndSet(new Human("AA", "BB", "CC", 99)));
        assertTrue(list.containsAll(ReflectionDemo.getGetAndSet(new Human("AA", "BB", "CC", 99))));
    }
}
