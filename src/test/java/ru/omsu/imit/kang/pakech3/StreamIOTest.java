package ru.omsu.imit.kang.pakech3;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;
import java.util.*;

import static org.junit.Assert.fail;
import static ru.omsu.imit.kang.pakech3.StreamIO.getFilesWithCatalogues;
import static ru.omsu.imit.kang.pakech3.StreamIO.getListWithFileExpancion;

@RunWith(JUnit4.class)

public class StreamIOTest {

    @Test
    public void testReadAndWriteIntArrayToBinaryFile() {
        File file = new File("input");
        int[] arr = {100000, 1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] assertArr = {100000, 1, 2, 3, 4, 5, 7 ,6};
        StreamIO.writeIntArrayToBinaryFile(file, arr);
        arr = StreamIO.readIntArrayFromBinaryFile(file, 8);
        Assert.assertArrayEquals(assertArr, arr);
    }

    @Test
    public void testReadAndWriteCharArrayToBinaryFile() {
        File file = new File("input");
        int[] arr = {100000, 2, 4, 7, 5, 3, 1};
        int[] expected = {100000, 2, 4, 7, 5, 3, 1};
        StreamIO.writeCharArrayToBinaryFile(file, arr);
        int[] actual = StreamIO.readCharArrayFromBinaryFile(file, 7);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testReadIntArrayUsingRAF() {
        File file = new File("input.txt");
        int[] arr = {0, 1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] expected = {5, 4, 3, 2, 1, 0};
        StreamIO.writeIntArrayToBinaryFile(file, arr);
        int[] actual = StreamIO.readIntArrayFromPositionFromBinaryFile(file, 8, 6);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testGetListWithFileExpancion() {
        List<String> list = getListWithFileExpancion(".txt", "C:\\Users\\A\\Desktop\\imit-4sem-java");
        Assert.assertEquals(3, list.size());
        Assert.assertEquals("input.txt", list.get(0));
        Assert.assertEquals("k1k.txt", list.get(1));
        Assert.assertEquals("loo.txt", list.get(2));
    }

    @Ignore
    @Test
    public void testGetFilesWithCatalogues() {
        List<String> list = getFilesWithCatalogues(".txt", "C:\\Users\\A\\Desktop\\imit-4sem-java");

        for (String str: list) {
            System.out.println(str);
        }

        Assert.assertEquals(true, true);
    }

    @Test
    public void saveHouseToCSV() {
        Person person1 = new Person("Александр", "Владимирович", "Петров", 13, 12, 1987);
        Person person2 = new Person("Ольга", "Мармеладова", "Сидорова", 13, 12, 1987);
        Person person3 = new Person("Игорь", "Петрович", "Васечкин", 13, 12, 1987);
        Person person4 = new Person("Сергей", "Бальзамов", "Золотов", 13, 12, 1987);

        List<Person> list1 = new ArrayList<>();
        list1.add(person1);
        List<Person> list2 = new ArrayList<>();
        list2.add(person2);
        List<Person> list3 = new ArrayList<>();
        list3.add(person3);
        list3.add(person4);

        Flat flat1 = new Flat(1, 40, list1);
        Flat flat2 = new Flat(2, 65, list2);
        Flat flat3 = new Flat(3, 58, list3);
        List<Flat> listFlat = new ArrayList<>();

        listFlat.add(flat1);
        listFlat.add(flat2);
        listFlat.add(flat3);
        Person senior = new Person("Сидор", "Сидорович", "Иванов", 01, 01, 1988);

        House house = new House("12345", "г. Омск, пр. Мира, 321", senior, listFlat);

        Assert.assertEquals(house.getList(), listFlat);
        Assert.assertEquals(house.getList().get(0), flat1);
        Assert.assertEquals(house.getList().get(1), flat2);
        Assert.assertEquals(house.getList().get(2), flat3);

        StreamIO.saveHouseToCSV(house);
    }

    @Test
    public void testSerializeAndDeserializeHouseFromBinaryFile() {
        File file = new File("input");
        Person person1 = new Person("Александр", "Владимирович", "Петров", 13, 12, 1987);
        Person person2 = new Person("Ольга", "Мармеладова", "Сидорова", 13, 12, 1987);
        Person person3 = new Person("Игорь", "Петрович", "Васечкин", 13, 12, 1987);
        Person person4 = new Person("Сергей", "Бальзамов", "Золотов", 13, 12, 1987);
        List<Person> list1 = new ArrayList<>();
        list1.add(person1);
        List<Person> list2 = new ArrayList<>();
        list2.add(person2);
        List<Person> list3 = new ArrayList<>();
        list3.add(person3);
        list3.add(person4);
        Flat flat1 = new Flat(1, 40, list1);
        Flat flat2 = new Flat(2, 65, list2);
        Flat flat3 = new Flat(3, 58, list3);
        List<Flat> listFlat = new ArrayList<>();
        listFlat.add(flat1);
        listFlat.add(flat2);
        listFlat.add(flat3);
        Person senior = new Person("Сидор", "Сидорович", "Иванов", 01, 01, 1988);
        House house = new House("12345", "г. Омск, пр. Мира, 321", senior, listFlat);


        StreamIO.serializeHouseToBinaryFile(file, house);
        House res = StreamIO.deserializeHouseFromBinaryFile(file);
        Assert.assertEquals(house, res);
    }

    @Test
    public void testWriteAndReadHouse() {
        File file = new File("input");
        House house = new House("32", "Lenina" , new Person("Akilbek", "Akilbekov", "Akilbekovich", 13, 10, 1994));
        StreamIO.writeHouse(file, house);
        House res = StreamIO.readHouse(file);
        Assert.assertEquals(house, res);
    }

}
